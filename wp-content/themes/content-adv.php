<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dell 5110
 * Date: 6/10/12
 * Time: 6:16 PM
 * To change this template use File | Settings | File Templates.

 */
?>
<?php
get_header();
?>
<div class="p-promo" id="page">
    <div class="navSub">
        <ul>
            <li><a href="#">3 MIỀN</a></li>
            <li><a href="#" class="active">3 MIỀN GOLD</a></li>
            <li><a href="#">LEON</a></li>
        </ul>
    </div>
    <div class="main">
        <h1>TVC</h1>
        <div class="tvc">
            <ul>
                <li>
                    <a href="#" class="thumb"><img alt="" src="images/demo/tvc_thumb.jpg"><span></span></a>
                    <a href="#">Phần Thưởng Vô Giá</a>
                </li>
                <li>
                    <a href="#" class="thumb"><img alt="" src="images/demo/tvc_thumb.jpg"><span></span></a>
                    <a href="#">Phần Thưởng Vô Giá</a>
                </li>
                <li>
                    <a href="#" class="thumb"><img alt="" src="images/demo/tvc_thumb.jpg"><span></span></a>
                    <a href="#">Phần Thưởng Vô Giá</a>
                </li>
                <li>
                    <a href="#" class="thumb"><img alt="" src="images/demo/tvc_thumb.jpg"><span></span></a>
                    <a href="#">Phần Thưởng Vô Giá</a>
                </li>
            </ul>
        </div>

        <h1>TIN BÀI</h1>
        <div class="promoList">
            <ul>
                <li>
                    <a href="#"><img alt="" src="images/demo/tvc_thumb.jpg" class="thumb"></a>
                    <p>Trong cuộc sống năng động mỗi ngày, thời gian quí như vàng...</p>
                    <a href="#" class="viewMore">Xem tiếp</a>
                </li>
                <li>
                    <a href="#"><img alt="" src="images/demo/tvc_thumb.jpg" class="thumb"></a>
                    <p>Trong cuộc sống năng động mỗi ngày, thời gian quí như vàng...</p>
                    <a href="#" class="viewMore">Xem tiếp</a>
                </li>
                <li>
                    <a href="#"><img alt="" src="images/demo/tvc_thumb.jpg" class="thumb"></a>
                    <p>Trong cuộc sống năng động mỗi ngày, thời gian quí như vàng...</p>
                    <a href="#" class="viewMore">Xem tiếp</a>
                </li>
            </ul>
        </div>

        <h1>KHUYẾN MÃI</h1>
        <div class="promoList">
            <ul>
                <li>
                    <a href="#"><img alt="" src="images/demo/tvc_thumb.jpg" class="thumb"></a>
                    <p>Trong cuộc sống năng động mỗi ngày, thời gian quí như vàng...</p>
                    <a href="#" class="viewMore">Xem tiếp</a>
                </li>
                <li>
                    <a href="#"><img alt="" src="images/demo/tvc_thumb.jpg" class="thumb"></a>
                    <p>Trong cuộc sống năng động mỗi ngày, thời gian quí như vàng...</p>
                    <a href="#" class="viewMore">Xem tiếp</a>
                </li>
                <li>
                    <a href="#"><img alt="" src="images/demo/tvc_thumb.jpg" class="thumb"></a>
                    <p>Trong cuộc sống năng động mỗi ngày, thời gian quí như vàng...</p>
                    <a href="#" class="viewMore">Xem tiếp</a>
                </li>
            </ul>
        </div>
    </div>

    <p class="cl"></p>
</div>