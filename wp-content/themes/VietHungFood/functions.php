<?php
 add_theme_support('post-thumbnails');
register_nav_menus( array(
    'main_top' => __( 'Menu top'),
) );
function get_cat_slug($cat_id) {
    $cat_id = (int) $cat_id;
    $category = &get_category($cat_id);
    return $category->slug;
}

function get_latest_post_html() {
    $content = "";
    //showposts hiển thị những bài viết mới nhất. Mặc định là 1.
    query_posts('showposts=1');
    while (have_posts()){
        the_post();
        $content .= "<p class='title'><a href='" . get_permalink() . "'>" . get_the_title() . "</a></p>\n" . "<p class='excerpt'>" . get_the_excerpt() . "</p>";
    }
    wp_reset_query();
    return $content;
}

/*
    * detect and display child category
    * finished and lose my work hu hu :((
    * $childsArray = (array($parentId, $childId, $display, $textData));
    */
/*
Hàm này liệt kê các Category có id cha (parents_id) là $prentsID
   @array $catData
   @int $prentsID optional, default 0
   return string Category HTML format
*/
function getCategory($catData, $prentsID= 0) {
    $catList = "<ul>";
    foreach($catData as $aCat) {
        if ($aCat['parents_id'] == $prentsID) {
            $catList .= "<li>".$aCat['name']."</li>";
            $catList .= getCategory($catData, $aCat['id']);
        }
    }
    $catList .= "</ul>";
    return $catList;
}

function getCurrentCatID(){

    global $wp_query;

    if(is_category() || is_single()){

        $cat_ID = get_query_var('cat');
    }

    return $cat_ID;
}

function get_first_image() {
    global $post, $posts;
    $first_img = '';
    ob_start();
    ob_end_clean();
    $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
    $first_img = $matches [1] [0];
    if(empty($first_img)){ //Defines a default image
        $first_img = "/images/default.jpg";
    }
    return $first_img;
}
if (class_exists('MultiPostThumbnails')) {
    new MultiPostThumbnails(
        array(
            'label' => 'Secondary Image',
            'id' => 'secondary-image',
            'post_type' => 'post'
        )
    );
}
?>
