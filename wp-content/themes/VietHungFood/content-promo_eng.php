<?php
/**
 * Created by JetBrains PhpStorm.
 * User: VietDev01
 * Date: 6/22/12
 * Time: 1:41 PM
 * To change this template use File | Settings | File Templates.
 * *Template name: Quảng cáo - khuyến mãi - Eng
 */
?>
<?php get_header(); ?>
<script type="text/javascript">
    selectNav(4);
</script>
<div class="p-promo" id="page">
    <div class="navSub">
        <ul>
            <?php
            $currentLink = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
            $categories = get_categories(array("parent"=>37,'hide_empty'=>0));

            foreach((Array)$categories as $category)
            {
                $cat_link = get_category_link( $category->term_id );
                if (strpos($cat_link, $currentLink)){
                    $currentCat = $category;
                }
                ?>
                <li><a href="<?php echo esc_url( $cat_link );?>"> <?php echo $category->cat_name; ?> </a></li>
                <?php
            }
            if(empty($currentCat) && !empty($categories)){
                $currentCat = $categories[0];
            }
            ?>
        </ul>
    </div>
<?php if(!empty($currentCat)){?>
    <?php  ?>
    <?php if(is_single()) {
        include (TEMPLATEPATH . '/single-promo.php');
        $postTemp = $wp_query->post;
        $catte = get_the_category($postTemp->ID);
        $listPost = get_posts(array("category"=>$catte[0]->term_id, "order_by"=>"post_date"));
        if(count($listPost)>1){
            ?>
            <br/><br/>
            <h2> Các tin khác: </h2>
            <div class="tvc"></div>
            <div class="latestNews">
                <ul>
                    <?php foreach((array)$listPost as $post){
                    if($post->ID != $postTemp->ID) { ?>
                        <li><a href="<?php the_permalink(); ?>"> <?php echo $post->post_title ?></a></li>
                        <?php  }
                } ?>
                </ul>
            </div>
            <?php } ?> </div>
        <?php } else{?>

    <div class="main">
        <?php $categories_item = get_categories(array("parent"=>$currentCat->cat_ID,'orderby'=>'order'));
        foreach((Array)$categories_item as $category_item){
            $listPost = get_posts(array("category"=>$category_item->term_id,"numberposts"=>8, "order_by"=>"post_date"));
            ?>
            <h1> <?php echo $category_item->cat_name; ?> </h1>
            <div class="tvc">
                <ul>
                    <?php for($i = 0; $i < count($listPost); ++$i){?>
                    <li>
                        <?php $post = get_post($listPost[$i]->ID);
                        if(strlen(get_the_post_thumbnail($post->ID)) > 0) { ?>
                            <a class="thumb" href="<?php the_permalink();?>">
                                <img alt="" <?php echo get_the_post_thumbnail($listPost[$i]->ID);?> </img><p> <?php echo $post->post_title; ?> </p>
                            </a>
                            <?php }else ?>
                            <a href="<?php the_permalink(); ?>"><?php echo $post->post_title ?></a>
                    </li>
                    <?php }?>
                </ul>
            </div>
            <!--            < ?php
         }elseif($category_item->cat_name == "TIN BÀI"){
                $listPost = get_posts(array("category"=>$category_item->term_id,"numberposts"=>4, "order_by"=>"post_date"));
                if(!empty($listPost)){ ?>
                    <h1>TIN BÀI</h1>
				<div class="promoList">
					<ul>
                        < ?php for($i = 0; $i < count($listPost); ++$i){?>
						<li>
                        <a href="< ?php echo post_permalink($listPost[$i]->ID)?>">
                            < ?php
                            if(strlen(get_the_post_thumbnail($listPost[$i]->ID)) > 0)
                            {?>
                                   <img  class="thumb" alt="" < ?php echo get_the_post_thumbnail($listPost[$i]->ID);?></img>
                                    < ?php } ?>
                            </a>
                            <p align="justify">< ?php echo $listPost[$i]->post_title; ?></p>
                            <a href="< ?php echo post_permalink($listPost[$i]->ID)?>" class="viewMore">Xem tiếp</a>
                        </li>
                        < ?php } ?>
					</ul>
				</div>
             < ?php   }
            }elseif($category_item->cat_name == "KHUYẾN MÃI"){
                $listPost = get_posts(array("category"=>$category_item->term_id,"numberposts"=>4, "order_by"=>"post_date"));
                if(!empty($listPost)){?>
                    <h1>KHUYẾN MÃI</h1>
				<div class="promoList">
					<ul>
                        < ?php for($i = 0; $i < count($listPost); ++$i){?>
                        <li>
                            <a href="< ?php echo post_permalink($listPost[$i]->ID)?>">
                                < ?php
                                if(strlen(get_the_post_thumbnail($listPost[$i]->ID)) > 0)
                                {?>
                                   <img  class="thumb" alt="" < ?php echo get_the_post_thumbnail($listPost[$i]->ID);?></img>
                                    < ?php } ?>
                            </a>
                            <p align="justify">< ?php echo $listPost[$i]->post_title; ?></p>
                            <a href="< ?php echo post_permalink($listPost[$i]->ID)?>" class="viewMore">Xem tiếp</a>
                        </li>
                        < ?php } ?>
					</ul>
                </div>
           < ?php }
            } -->
            <?php  }?>
    </div>
    <?php }} else{?>
    <h1>Posts are updating ...</h1>
<?php }?>


<p class="cl"></p>
</div>
<?php get_footer(); ?>



