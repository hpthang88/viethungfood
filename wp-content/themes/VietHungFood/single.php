<?php
    $category = get_the_category();
    $yourcat = get_category($category[0]);
    while($yourcat->category_parent > 0){
        $parentca =  $yourcat->category_parent;
        $yourcat = get_category($parentca);
    }
    if($parentca == 4){
        get_template_part( 'content','product');
    }else if($parentca == 5){
        get_template_part( 'content','promo');
    }else if($parentca == 6){
        get_template_part( 'content','health');
    }else if($parentca == 7 || $yourcat->cat_ID == 7){
        get_template_part( 'content','community');
    } else if($parentca == 37 || $yourcat->cat_ID == 37){
        get_template_part( 'content','promo_eng');
    } else if($parentca == 55 || $yourcat->cat_ID == 55){
        get_template_part( 'content','product_eng');
    }else if($parentca == 36 || $yourcat->cat_ID == 36){
        get_template_part( 'content','community_eng');
    }else if($parentca == 52 || $yourcat->cat_ID == 52){
        get_template_part( 'content','health_eng');
    }

?>