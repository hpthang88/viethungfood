<?php
/**
 * The template for displaying Category Archive pages.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>

		<section  id="primary">
			<div id="content" role="main" >

			<?php //if( have_posts() ) : ?>
					<?php
						$category_description = category_description();
						if ( ! empty( $category_description ) )
							echo apply_filters( 'category_archive_meta', '<div class="category-archive-meta">' . $category_description . '</div>' );
					?>
				</header>
                <?php if(is_category()) {
                    $cat = get_query_var('cat');
                    $categories = get_cat_slug($cat);
                    if($categories =="cong-ty")
                        get_template_part( 'content','gioithieu');
                    elseif($categories =="thuong-hieu-san-pham")
                        get_template_part( 'content','product');
                    elseif($categories == "quang-cao-khuyen-mai")
                        get_template_part( 'content','promo');
                    elseif($categories == "suc-khoe")
                        get_template_part( 'content','health');
                    elseif($categories == "cong-dong")
                        get_template_part( 'content','community');
                    elseif($categories == "advertisement-promotion")
                        get_template_part( 'content','promo_eng');
                    elseif($categories == "brands-products")
                        get_template_part( 'content','product_eng');
                    elseif($categories == "helth")
                        get_template_part( 'content','health_eng');
                    elseif($categories == "community")
                        get_template_part( 'content','community_eng');
                    else{
                        $yourcat = get_category($cat);
                         while($yourcat->category_parent !=0)
                        {
                            $parentca =  $yourcat->category_parent;
                            $yourcat = get_category($parentca);

                        }
                            $sdd = get_cat_slug($parentca);
                        if($sdd =="cong-ty")
                            get_template_part( 'content','gioithieu');
                            elseif($sdd =="thuong-hieu-san-pham")
                                get_template_part( 'content','product');
                            elseif($sdd == "quang-cao-khuyen-mai")
                                get_template_part( 'content','promo');
                            elseif($sdd == "suc-khoe")
                                get_template_part( 'content','health');
                            elseif($sdd == "cong-dong")
                                get_template_part( 'content','community');
                            elseif($sdd == "advertisement-promotion")
                                get_template_part( 'content','promo_eng');
                            elseif($sdd == "brands-products")
                                get_template_part( 'content','product_eng');
                            elseif($sdd == "helth")
                                get_template_part( 'content','health_eng');
                            elseif($sdd == "community")
                                get_template_part( 'content','community_eng');
                        }
                }?>
				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>

				<?php endwhile; ?>
			<?php //else : ?>

				<!--<article id="post-0" class="post no-results not-found">
					<header class="entry-header">
						<h1 class="entry-title"><?php //_e( 'Không tìm thấy bài viết nào thuộc sản phẩm này!', 'VietHungFood' ); ?></h1>
					</header>--><!-- .entry-header -->

					<!--<div class="entry-content">
						<p><?php //_e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'VietHungFood' ); ?></p>

					</div>--><!-- .entry-content -->
				<!--</article>--><!-- #post-0 -->

			<?php //endif; ?>

			</div><!-- #content -->
		</section><!-- #primary -->
<?php get_footer(); ?>
