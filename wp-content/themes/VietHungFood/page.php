<?php get_header(); ?>
<div class="entry">
    <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="entry-content">
            <?php the_content(); ?>
        </div><!-- .entry-content -->
    </div>
    <?php endwhile; // end of the loop. ?>
</div><!--/main-->

<?php get_footer(); ?>