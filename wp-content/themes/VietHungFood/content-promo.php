<?php
/**
 * Created by JetBrains PhpStorm.
 * User: VietDev01
 * Date: 6/4/12
 * Time: 2:40 PM
 * To change this template use File | Settings | File Templates.
 * *Template name: Quảng cáo - khuyến mãi
 */?>
<?php get_header(); ?>
<script type="text/javascript">
    selectNav(4);

</script>
<div class="p-promo" id="page">
    <div class="navSub">
        <ul>
            <?php
                $currentLink = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
                $categories = get_categories(array("parent"=>5,'hide_empty'=>0));

                foreach((Array)$categories as $category)
                {
                    $cat_link = get_category_link( $category->term_id );
                    if (strpos($cat_link, $currentLink)){
                        $currentCat = $category;
                    }
                    ?>
                    <li><a class="men_<? echo $category->term_id ?>" href="<?php echo esc_url( $cat_link );?>"> <?php echo $category->cat_name; ?> </a></li>
                    <?php
                }
                if(empty($currentCat) && !empty($categories)){
                    $currentCat = $categories[0];
                }
            ?>
        </ul>
    </div>
    <?php if(!empty($currentCat)){?>
    <?php  ?>
        <?php if(is_single()) {
            include (TEMPLATEPATH . '/single-promo.php');
        $postTemp = $wp_query->post;
        $catte = get_the_category($postTemp->ID);
        $listPost = get_posts(array("category"=>$catte[0]->term_id, "order_by"=>"post_date"));
        if(count($listPost)>1){
        ?>
        <br/><br/>
        <h2> Các tin khác: </h2>
            <div class="tvc"></div>
        <div class="latestNews">
            <ul>
                <?php foreach((array)$listPost as $post){
                    if($post->ID != $postTemp->ID) { ?>
                        <li><a href="<?php the_permalink(); ?>"> <?php echo $post->post_title ?></a></li>
                    <?php  }
                } ?>
            </ul>
        </div>
        <?php } ?> </div>
        <?php } else{?>

        <div class="main">
        <?php $categories_item = get_categories(array("parent"=>$currentCat->cat_ID,'orderby'=>'order'));
           if($categories_item == null){
               echo "Hiện tại không có bài viết trong chuyên mục này!";
           }else{
        foreach((Array)$categories_item as $category_item){
                $listPost = get_posts(array("category"=>$category_item->term_id,"numberposts"=>8, "order_by"=>"post_date"));
                ?>
                <h1> <?php echo $category_item->cat_name; ?> </h1>
                <div class="tvc">
                    <ul>
                        <?php for($i = 0; $i < count($listPost); ++$i){?>
                        <li>
                            <?php $post = get_post($listPost[$i]->ID);
                            if(strlen(get_the_post_thumbnail($post->ID)) > 0) { ?>
                            <a class="thumb" href="<?php the_permalink();?>">
                              <img alt="" <?php echo get_the_post_thumbnail($listPost[$i]->ID);?> </img><p> <?php echo $post->post_title; ?> </p>
                            </a>
                            <?php }else ?>
                            <a href="<?php the_permalink(); ?>"><?php echo $post->post_title ?></a>
                        </li>
                        <?php }?>
                    </ul>
                </div>
      <?php  }}?>
    </div>
    <?php }} else{
    echo "Hiện tại không có bài viết trong chuyên mục này!";
    }?>


    <p class="cl"></p>
    </div>
<script type="text/javascript">
    $(document).ready(function(){
        $(".men_" + "<? if( $catte[0]!= null){ echo $catte[0]->parent; }else{ echo $currentCat->term_id;} ?>").addClass("active");
    })
</script>
<?php get_footer(); ?>

