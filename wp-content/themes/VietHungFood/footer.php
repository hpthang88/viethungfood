<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>

</div><!-- #main -->

<footer>


    <div  >

        <p >© Copyright <label id="currentYear">2012</label> by Viet Hung Food Industry Company Limited</p>
        <p>Phan Van Doi Road, Tien Lan Hamlet, Ba Diem Ward, Hoc Mon District, Ho Chi Minh City, Vietnam</p>
        <p>
            <strong>Tel:</strong> +84 8 37 125 697 
            <span><strong>Fax:</strong> +84 8 37 125 699</span>
            <span><strong>Web:</strong> <a href="http://www.viethungfood.com">www.viethungfood.com</a></span>
        </p>
    </div>
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>