<?php
/**
 * Created by JetBrains PhpStorm.
 * User: VietDev01
 * Date: 6/4/12
 * Time: 9:41 AM
 * To change this template use File | Settings | File Templates.
 * *Template Name: Thương hiệu - sản phẩm
 */
?>
<?php get_header(); ?>
<script type="text/javascript">
    selectNav(3);
</script>
    <div class="p-product" id="page">

        <div class="navSub" >
            <ul id="navSub">
                <?php
                $currentLink = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
                $categories = get_categories(array("parent"=>4,'hide_empty'=>0));
                $post = $wp_query->post;
                $catofpost23 = get_the_category($post->post->ID);
                print_r($catofpost23);
                $catID = $catofpost23[0]->category_parent;

                foreach((Array)$categories as $category)
                { ?>
                    <li class="cat2">
                        <?php  $cat_link_post = get_category_link( $category->cat_ID );
                        ?>
                        <a  class="men_<? echo $category->term_id ?>"  href="<?php echo get_category_link( $category->cat_ID ) ?>" id="cat_<?php echo $category->cat_ID; ?>"> <?php echo $category->cat_name; ?> </a>
                        <?php
                        $categories_item = get_categories(array("parent"=>$category->term_id,'hide_empty'=>0));
                        ?>
                        <ul>
                            <?php
                            $catofpost = get_the_category($wp_query->post->ID);
                            foreach((Array)$categories_item as $currentCat_item)
                            {
                                $cat_link = get_category_link( $currentCat_item->term_id );
                                if ($cat_link == get_category_link($catofpost[0]->term_id) || strpos($cat_link, $currentLink)){
                                    $currentCat = $currentCat_item;
                                } ?>
                                <li><a  href="<?php echo esc_url( $cat_link );?>"> <?php echo $currentCat_item->cat_name; ?> </a></li>
                                <?php } ?>
                        </ul>
                    </li>
                    <?php    }
                //$first = false;
                if(empty($currentCat) && !empty($categories)){
                    //$first = true;
                    $ls = get_categories(array("parent"=>$categories[-1]->term_id,'hide_empty'=>0));
                    $currentCat123 = $ls[0];
                }?>
            </ul>
        </div>
    <div class="main">
        <?php
        if(!empty($currentCat123)){
            $listPost = get_posts(array("category"=>$currentCat123->term_id,"numberposts"=>1, "order_by"=>"post_date"));
            }
        elseif($catID == 4){
            $listPost = get_posts(array("category"=>$currentCat->term_id,"numberposts"=>1, "order_by"=>"post_date"));
        }
        elseif(!empty($currentCat)){
            $listPost = get_posts(array("category"=>$currentCat->term_id,"numberposts"=>100, "order_by"=>"post_date"));
        } ?>
                <?php
                if(!empty($listPost)){
                    ?>
                    <?php include (TEMPLATEPATH . '/single-product.php');
                    //$catofpost23 = get_the_category($listPost[0]->post->ID);
                    ?>
                    <div class="ct">
                        <?php

                        if (count($listPost) > 1){
                            ?>
                            <br/>
                            <div class="productSlide infiniteCarousel" style=" top: 50px;"> <!-- Hiển thị bài post thuộc category -->
                                <div class="slideCover wrapper">
                                    <ul>
                                        <?php
                                        for($i = 0; $i < count($listPost); ++$i){
                                            ?>
                                            <li>
                                                <a href="<?php echo post_permalink($listPost[$i]->ID)?>">
                                                    <?php
                                                    // $post = $wp_query->post;
                                                    // if ($listPost[$i]->ID != $post->ID){
                                                    if(strlen(get_the_post_thumbnail($listPost[$i]->ID)) > 0)
                                                    {?><div id="smallObject">
                                                        <?php echo get_the_post_thumbnail($listPost[$i]->ID, 'full');?></div><p><?php echo $listPost[$i]->post_title;?></p>
                                                        <?php
                                                    }else{
                                                        echo $listPost[$i]->post_title;
                                                    }?>
                                                </a>
                                            </li>
                                            <?php
                                            //  }
                                        }
                                        ?>
                                    </ul>

                                </div>
                                <a href="#" class="prev"></a>
                                <a href="#" class="next"></a>
                            </div>
                            <?php
                        }?>
                    </div>

                    <?php }?>
            </div>
            <p class="cl"></p>
            <?php

        ?></div>
    </div>

        <script type="text/javascript">
            function activeSubNav(strobj){
                if ($("#"+strobj).attr('class') != 'active') {
                    $("#navSub li ul").slideUp();
                    $("#"+ strobj).next().slideToggle();
                    $("#navSub li a").removeClass('active');
                    $("#" + strobj).addClass('active');
                }
            }
            function conplase(){
                $("#navSub li ul").slideUp();
            }
            function removefiximg(){
                $(".slideCover img").each(function(){
                    $(this).attr("width", "");
                    $(this).attr("height", "");
                });
            }
            $(document).ready(function(){
                removefiximg();
            });

            $(document).ready(function(){
                $(".men_" + "<? echo $catofpost23[0]->parent;  ?>").addClass("active");

            })
        </script>
<?php get_footer(); ?>