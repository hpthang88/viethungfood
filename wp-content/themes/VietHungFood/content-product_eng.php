<?php
/**
 * Created by JetBrains PhpStorm.
 * User: VietDev01
 * Date: 6/22/12
 * Time: 1:39 PM
 * To change this template use File | Settings | File Templates.
 * *Template Name: Thương hiệu - sản phẩm - Eng
 */
?>
<?php get_header(); ?>
<script type="text/javascript">
    selectNav(3);
</script>
<div class="p-product" id="page">

    <div class="navSub" >
        <ul id="navSub">
            <?php
            $currentLink = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
            $categories = get_categories(array("parent"=>55,'hide_empty'=>0));

            foreach((Array)$categories as $category)
            { ?>
                <li class="cat2">
                    <a  href="#" id="cat_<?php echo $category->cat_ID; ?>"> <?php echo $category->cat_name; ?> </a>
                    <?php
                    $categories_item = get_categories(array("parent"=>$category->term_id,'hide_empty'=>0));
                    ?>
                    <ul>
                        <?php
                        $catofpost = get_the_category($wp_query->post->ID);
                        foreach((Array)$categories_item as $currentCat_item)
                        {
                            $cat_link = get_category_link( $currentCat_item->term_id );
                            if ($cat_link == get_category_link($catofpost[0]->term_id) || strpos($cat_link, $currentLink)){
                                $currentCat = $currentCat_item;
                            } ?>
                            <li><a href="<?php echo esc_url( $cat_link );?>"> <?php echo $currentCat_item->cat_name; ?> </a></li>
                            <?php } ?>
                    </ul>
                </li>
                <?php
            }
            //$first = false;
            if(empty($currentCat) && !empty($categories)){
                //$first = true;
                $ls = get_categories(array("parent"=>$categories[0]->term_id,'hide_empty'=>0));
                $currentCat = $ls[0];
            }
            ?>
        </ul>
    </div>
    <?php
    //print_r($wp_query->post);
    if(!empty($currentCat)){
        $listPost = get_posts(array("category"=>$currentCat->term_id,"numberposts"=>10000, "order_by"=>"post_date"));
        ?>
        <div class="main">
            <?php
            if(!empty($listPost)){
                ?>
                <?php include (TEMPLATEPATH . '/single-product.php'); ?>
                <div class="ct">
                    <?php
                    if (count($listPost) > 0){
                        ?>

                        <div class="productSlide infiniteCarousel" style=" top: 50px;"> <!-- Hiển thị bài post thuộc category -->
                            <div class="slideCover wrapper">
                                <ul>
                                    <?php
                                    for($i = 0; $i < count($listPost); ++$i){
                                        ?>
                                        <li>
                                            <a href="<?php echo post_permalink($listPost[$i]->ID)?>">
                                                <?php
                                                // $post = $wp_query->post;
                                                // if ($listPost[$i]->ID != $post->ID){
                                                if(strlen(get_the_post_thumbnail($listPost[$i]->ID)) > 0)
                                                {?>
                                                    <?php echo get_the_post_thumbnail($listPost[$i]->ID, 'full',array('style' => 'height: 71px;width:70px;'));?><p><?php echo $listPost[$i]->post_title;?></p>
                                                    <?php
                                                }else{
                                                    echo $listPost[$i]->post_title;
                                                }?>
                                            </a>
                                        </li>
                                        <?php
                                        //  }
                                    }
                                    ?>
                                </ul>

                            </div>
                            <a href="#" class="prev"></a>
                            <a href="#" class="next"></a>
                        </div>
                        <?php
                    }?>
                </div>

                <?php }else{?>
                <h1>Bài viết đang được cập nhật ...</h1>
                <?php }?>
        </div>
        <p class="cl"></p>
        <?php
    }
    ?>
</div>


<script type="text/javascript">
    function activeSubNav(strobj){
        if ($("#"+strobj).attr('class') != 'active') {
            $("#navSub li ul").slideUp();
            $("#"+ strobj).next().slideToggle();

            $("#navSub li a").removeClass('active');
            $("#" + strobj).addClass('active');

        }
    }
    function conplase(){
        $("#navSub li ul").slideUp();
    }

</script>
<?php get_footer(); ?>