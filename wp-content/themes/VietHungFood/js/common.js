/*
 * @version 1.0
 */

function selectNav(order){
    var i = 1;
    $("#nav ul li").each(function(){
        if(order == i){
            $(this).children().addClass("active");
        }
        ++i;
    });
}

$(document).ready(function(){
						   
	// Get current year
	now = new Date;
	theYear = now.getYear();
	if (theYear < 1900)	theYear = theYear+1900;
	$('#currentYear').html(theYear);
	
	
	$('.nav li:last').css({ background:"none" });
	$('.nav a:last').css({ paddingRight:"19px"});
	
	
	$(".iframe").colorbox({iframe:true, width:"1055", height:"850"});
	
	
	var detectPage = $('#page').attr('class');
		detectPage = detectPage.substring(2, detectPage.length);
		switch (detectPage){
			case "home":
				
				break;
				
			case "about":
				$('.copy').jScrollPane();
				break;
		}
		
		
	$('.infiniteCarousel').infiniteCarousel();
	
	
	ginslider();
	
});





var ginSpeed		= 800;
var ginItemEach		= 1;
var ginCurrent		= 1;
var ginItemWidth	= 640;
var ginItemHeight	= 219;
var ginDelay 		= 5000;
var ginPause 		= false;
var ginToggle		= true;
var ginDotClicked	= false;
var ginAuto;
var ginSum;
var ginTransition	= "slide";	// slide fade
var ginEasing		= "easeOutQuint";
var ginItemClass 	= ".rotateCopy";
var ginNextClass 	= ".next";
var ginPrevClass 	= ".prev";
var ginDotClass 	= ".slideNav";
var ginCoverClass	= ".port_slideshow";

function ginslider(){
	
	// Count total item	
		ginSum=$(ginItemClass).length;
	
	//	Preload
		$('.port_view_all img').each(function(i){
			var getSrcImg = $('.port_view_all img:eq('+i+')').attr('src');			
			var Img = new Image();			
		
			Img.onerror = function(){
				//fn.error('This image cannot download!');
				return false;
			}		
			var src = getSrcImg;
			Img.onload = function(){ 
				//$('.loading').hide();
				 $('.port_view_all img:eq('+i+')').fadeIn();
				
				Img.onload=function(){};
			}	
			Img.src = src;
		});
		$('.port_view_all').css({
			width: ginItemWidth*ginSum
		});
	
	// Init dot navigation	
		ginslider_dot();	
	
	// Show first item
		$(ginItemClass+':eq('+ginCurrent+')').stop(true, true).fadeIn(ginSpeed);	
	
	// Init items's position
		switch (ginTransition){
			case "slide":
				for(var i=0; i<ginSum ;i++){
					$(ginItemClass+':eq('+i+')').attr('rel',(i*(Number(ginItemWidth)))).css("left",(i*(Number(ginItemWidth))));
				}
				break;
			case "fade":
				for(var i=0; i<ginSum ;i++){
					$(ginItemClass).attr('rel',(i*(Number(ginItemWidth)))).css("left",0).hide();
					$(ginItemClass+':first').show();
				}
				break;
		}

	// Bind prev/next button
		$(ginNextClass).live("click", ginNext);
		$(ginPrevClass).live("click", ginPrev);

	// Set dimension of div cover
		$(ginCoverClass).width(Number(ginItemEach * (ginItemWidth)));
	
	if ((ginSum>1) && (ginPause==false) && ( ginToggle==true)) 
		ginAuto=setTimeout("ginsliderPlay()",ginDelay);
}

function ginsliderPlay(){
	ginNext();
	clearTimeout(ginAuto);
	ginAuto=setTimeout("ginsliderPlay()",ginDelay);
}

function ginAutoNext(){
	ginsliderPlay();
}

function ginNext(){
	onSlide = true;	
	
	if (ginDotClicked == false){
		if(ginCurrent<(ginSum/ginItemEach)){
			
			//reset pagination
			clearTimeout(resetHD);
			$(ginDotClass+' a.active').removeClass('active').addClass('inactive right');
			$(ginDotClass+' a:eq('+ginCurrent+')').addClass('active left');
			resetHD = setTimeout("resetHomeDot()",400);
			
			//$('.btn_prev a').show();
			
			switch (ginTransition){
				case "slide":
					$('.port_view_all').stop(true,true).animate({left:'-='+(Number(ginItemWidth))*ginItemEach+'px'}, ginSpeed, ginEasing, function(){				
						 onSlide = false;
					});
					break;
				case "fade":
					$(ginItemClass).stop(true, true).fadeOut(ginSpeed);
					$(ginItemClass+':eq('+ginCurrent+')').stop(true, true).fadeIn(ginSpeed);	
					break;
			}
			
			ginCurrent++;
		}
		else{
			
			switch (ginTransition){
				case "slide":
					$('.port_view_all').stop(true,true).animate({left:'+='+(Number(ginItemWidth*(ginSum-1)))+'px' }, ginSpeed, ginEasing);
					ginCurrent=1;
					break;
				case "fade":
					ginCurrent=0;
					$(ginItemClass).stop(true, true).fadeOut(ginSpeed);
					$(ginItemClass+':eq('+ginCurrent+')').stop(true, true).fadeIn(ginSpeed);	
					ginCurrent=1;
					break;
			}
					
			//reset pagination
			clearTimeout(resetHD);
			$(ginDotClass+' a.active').removeClass('active').addClass('inactive right');
			$(ginDotClass+' a:eq(0)').addClass('active left');
			resetHD = setTimeout("resetHomeDot()",400);
			
		}
	}
	else{	
		alert(' not use for now ');
		//clearTimeout(ginAuto);
		var getTargetItem 	= ginCurrent;
		var getTargetItemW 	= getTargetItem*ginItemWidth;
		var getCurrentItem 	= (ginCurrent-1)*ginItemWidth;
		var gotoItem 		= getTargetItemW-getCurrentItem;
		var gotoAmount		= Math.abs(gotoItem);
		
		
		if (gotoItem>0){
			$(ginItemClass).stop(true,true).animate({left:'-='+gotoAmount+'px' }, ginSpeed, ginEasing,
				function(){
					ginCurrent = getTargetItem+1;
					$('.disableLayer').hide();
				}
			);
		}
		else{
			$(ginItemClass).stop(true,true).animate({left:'+='+gotoAmount+'px' }, ginSpeed, ginEasing,
				function(){
					ginCurrent = getTargetItem+1;
					$('.disableLayer').hide();
				}
			);
		}
		
		ginDotClicked = false;
	}
	
	clearTimeout(ginAuto);
	if ((ginPause==false) && ( ginToggle==true)) ginAuto=setTimeout("ginsliderPlay()",ginDelay);
	return false;
}

function ginPrev(){
	clearTimeout(ginAuto);
	clearTimeout(resetHD);
	onSlide = true;
	
	if(ginCurrent>1){	
		
		switch (ginTransition){
			case "slide":
				$('.port_view_all').stop(true,true).animate({left:'+='+(Number(ginItemWidth))*ginItemEach+'px'}, ginSpeed, ginEasing, function(){			
					 onSlide = false;
				});
				
				ginCurrent--;
				break;
			case "fade":
				ginCurrent--;
				$(ginItemClass).stop(true, true).fadeOut(ginSpeed);
				$(ginItemClass+':eq('+(ginCurrent-1)+')').stop(true, true).fadeIn(ginSpeed);	
				break;
		}
		
			
		
		//reset pagination
		var t_ginCurrent = ginCurrent-1;
		$(ginDotClass+' a.active').removeClass('active').addClass('inactive left');
		$(ginDotClass+' a:eq('+t_ginCurrent+')').addClass('active right');
		resetHD = setTimeout("resetHomeDot()",400);
	}
	else{
		
		switch (ginTransition){
			case "slide":
				$('.port_view_all').stop(true,true).animate({left:'-='+(Number(ginItemWidth*(ginSum-1)))+'px' }, ginSpeed, ginEasing);				
				ginCurrent=ginSum;
				break;
			case "fade":
				ginCurrent=ginSum-1;
				$(ginItemClass).stop(true, true).fadeOut(ginSpeed);
				$(ginItemClass+':eq('+ginCurrent+')').stop(true, true).fadeIn(ginSpeed);
				ginCurrent=ginSum;
				break;
		}
		
		//reset pagination
		var t_numItems = ginSum-1;
		$(ginDotClass+' a.active').removeClass('active').addClass('inactive left');
		$(ginDotClass+' a:eq('+t_numItems+')').addClass('active right');
		resetHD = setTimeout("resetHomeDot()",400);
		
	}
	
	clearTimeout(ginAuto);
	if ((ginPause==false) && ( ginToggle==true)) ginAuto=setTimeout("ginsliderPlay()",ginDelay);
	return false;
}

function ginslider_dot(){	/* generate pagination */
	if (ginSum>1){
		for (var tempPaging=0; tempPaging<ginSum; tempPaging++){
			$('.slideNav a:eq('+tempPaging+')').attr('alt',tempPaging);
		}
		
		$(ginDotClass+' a:first').addClass('active');
		$(ginDotClass+' a').click(function(){
			
			$(ginDotClass+' a').removeClass('active');
			$(this).addClass('active');
			$('.disableLayer').show();
			
			switch (ginTransition){
				case "slide":
					var getTargetItem 	= Number($(this).attr('alt'));
					var getTargetItemW 	= getTargetItem*ginItemWidth;
					var getCurrentItem 	= $(ginItemClass+':eq('+ Number(ginCurrent-1) +')').attr('rel');
					var gotoItem 		= getTargetItemW-getCurrentItem;
					var gotoAmount		= Math.abs(gotoItem);
					
					if (gotoItem>0){
						$('.port_view_all').stop(true,true).animate({left:'-='+gotoAmount+'px' }, ginSpeed, ginEasing,  
							function(){
								ginCurrent = getTargetItem+1;
								$('.disableLayer').hide();
								
							}
						);
					}
					else{
					  $('.port_view_all').stop(true,true).animate({left:'+='+gotoAmount+'px' }, ginSpeed, ginEasing,  
							function(){
								ginCurrent = getTargetItem+1;
								$('.disableLayer').hide();
								
							}
						);
					}
					clearTimeout(ginAuto);
					if ((ginPause==false) && ( ginToggle==true)) ginAuto=setTimeout("ginAutoNext()",ginDelay);
					break;
					
				case "fade":
					clearTimeout(ginAuto);
					ginCurrent = Number($(this).attr('alt'));
					if ((ginPause==false) && ( ginToggle==true)) ginAuto=setTimeout("ginAutoNext()",0);
					$('.disableLayer').hide();
					break;
			}			
			return false;
		});
	}
};

var resetHD;
function resetHomeDot(){
	$('.port_dotlink .active').removeClass('left right').parent().find('.inactive').removeClass('inactive right left');
};





$.fn.infiniteCarousel = function () {

    function repeat(str, num) {
        return new Array( num + 1 ).join( str );
    }
  
    return this.each(function () {
        var $wrapper = $('> div', this).css('overflow', 'hidden'),
            $slider = $wrapper.find('> ul'),
            $items = $slider.find('> li'),
            $single = $items.filter(':first'),
            
            singleWidth = $single.outerWidth(), 
            visible = Math.ceil($wrapper.innerWidth() / singleWidth), 
            currentPage = 1,
            pages = Math.ceil($items.length / visible);            


        // 1. Pad so that 'visible' number will always be seen, otherwise create empty items
        if (($items.length % visible) != 0) {
            $slider.append(repeat('<li class="empty" />', visible - ($items.length % visible)));
            $items = $slider.find('> li');
        }

        // 2. Top and tail the list with 'visible' number of items, top has the last section, and tail has the first
        $items.filter(':first').before($items.slice(- visible).clone().addClass('cloned'));
        $items.filter(':last').after($items.slice(0, visible).clone().addClass('cloned'));
        $items = $slider.find('> li'); // reselect
        
        // 3. Set the left position to the first 'real' item
        $wrapper.scrollLeft(singleWidth * visible);
        
        // 4. paging function
        function gotoPage(page) {
            var dir = page < currentPage ? -1 : 1,
                n = Math.abs(currentPage - page),
                left = singleWidth * dir * visible * n;
            
            $wrapper.filter(':not(:animated)').animate({
                scrollLeft : '+=' + left
            }, 500, function () {
                if (page == 0) {
                    $wrapper.scrollLeft(singleWidth * visible * pages);
                    page = pages;
                } else if (page > pages) {
                    $wrapper.scrollLeft(singleWidth * visible);
                    // reset back to start position
                    page = 1;
                } 

                currentPage = page;
            });                
            
            return false;
        }
        
        //$wrapper.after('<a class="arrow back">&lt;</a><a class="arrow forward">&gt;</a>');
        
        // 5. Bind to the forward and back buttons
        $('a.prev', this).click(function () {
            return gotoPage(currentPage - 1);                
        });
        
        $('a.next', this).click(function () {
            return gotoPage(currentPage + 1);
        });
        
        // create a public interface to move to a specific page
        $(this).bind('goto', function (event, page) {
            gotoPage(page);
        });
    });  
};






/*
 * jQuery Easing v1.3 - http://gsgd.co.uk/sandbox/jquery/easing/
 *
 * Uses the built in easing capabilities added In jQuery 1.1
 * to offer multiple easing options
 *
 * TERMS OF USE - jQuery Easing
 * 
 * Open source under the BSD License. 
 * 
 * Copyright © 2008 George McGinley Smith
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 
 * Redistributions of source code must retain the above copyright notice, this list of 
 * conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list 
 * of conditions and the following disclaimer in the documentation and/or other materials 
 * provided with the distribution.
 * 
 * Neither the name of the author nor the names of contributors may be used to endorse 
 * or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED 
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE. 
 *
*/

// t: current time, b: begInnIng value, c: change In value, d: duration
jQuery.easing['jswing'] = jQuery.easing['swing'];

jQuery.extend( jQuery.easing,
{
	def: 'easeOutQuad',
	swing: function (x, t, b, c, d) {
		//alert(jQuery.easing.default);
		return jQuery.easing[jQuery.easing.def](x, t, b, c, d);
	},
	easeInQuad: function (x, t, b, c, d) {
		return c*(t/=d)*t + b;
	},
	easeOutQuad: function (x, t, b, c, d) {
		return -c *(t/=d)*(t-2) + b;
	},
	easeInOutQuad: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t + b;
		return -c/2 * ((--t)*(t-2) - 1) + b;
	},
	easeInCubic: function (x, t, b, c, d) {
		return c*(t/=d)*t*t + b;
	},
	easeOutCubic: function (x, t, b, c, d) {
		return c*((t=t/d-1)*t*t + 1) + b;
	},
	easeInOutCubic: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t*t + b;
		return c/2*((t-=2)*t*t + 2) + b;
	},
	easeInQuart: function (x, t, b, c, d) {
		return c*(t/=d)*t*t*t + b;
	},
	easeOutQuart: function (x, t, b, c, d) {
		return -c * ((t=t/d-1)*t*t*t - 1) + b;
	},
	easeInOutQuart: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t*t*t + b;
		return -c/2 * ((t-=2)*t*t*t - 2) + b;
	},
	easeInQuint: function (x, t, b, c, d) {
		return c*(t/=d)*t*t*t*t + b;
	},
	easeOutQuint: function (x, t, b, c, d) {
		return c*((t=t/d-1)*t*t*t*t + 1) + b;
	},
	easeInOutQuint: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t*t*t*t + b;
		return c/2*((t-=2)*t*t*t*t + 2) + b;
	},
	easeInSine: function (x, t, b, c, d) {
		return -c * Math.cos(t/d * (Math.PI/2)) + c + b;
	},
	easeOutSine: function (x, t, b, c, d) {
		return c * Math.sin(t/d * (Math.PI/2)) + b;
	},
	easeInOutSine: function (x, t, b, c, d) {
		return -c/2 * (Math.cos(Math.PI*t/d) - 1) + b;
	},
	easeInExpo: function (x, t, b, c, d) {
		return (t==0) ? b : c * Math.pow(2, 10 * (t/d - 1)) + b;
	},
	easeOutExpo: function (x, t, b, c, d) {
		return (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) + 1) + b;
	},
	easeInOutExpo: function (x, t, b, c, d) {
		if (t==0) return b;
		if (t==d) return b+c;
		if ((t/=d/2) < 1) return c/2 * Math.pow(2, 10 * (t - 1)) + b;
		return c/2 * (-Math.pow(2, -10 * --t) + 2) + b;
	},
	easeInCirc: function (x, t, b, c, d) {
		return -c * (Math.sqrt(1 - (t/=d)*t) - 1) + b;
	},
	easeOutCirc: function (x, t, b, c, d) {
		return c * Math.sqrt(1 - (t=t/d-1)*t) + b;
	},
	easeInOutCirc: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return -c/2 * (Math.sqrt(1 - t*t) - 1) + b;
		return c/2 * (Math.sqrt(1 - (t-=2)*t) + 1) + b;
	},
	easeInElastic: function (x, t, b, c, d) {
		var s=1.70158;var p=0;var a=c;
		if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
		if (a < Math.abs(c)) { a=c; var s=p/4; }
		else var s = p/(2*Math.PI) * Math.asin (c/a);
		return -(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
	},
	easeOutElastic: function (x, t, b, c, d) {
		var s=1.70158;var p=0;var a=c;
		if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
		if (a < Math.abs(c)) { a=c; var s=p/4; }
		else var s = p/(2*Math.PI) * Math.asin (c/a);
		return a*Math.pow(2,-10*t) * Math.sin( (t*d-s)*(2*Math.PI)/p ) + c + b;
	},
	easeInOutElastic: function (x, t, b, c, d) {
		var s=1.70158;var p=0;var a=c;
		if (t==0) return b;  if ((t/=d/2)==2) return b+c;  if (!p) p=d*(.3*1.5);
		if (a < Math.abs(c)) { a=c; var s=p/4; }
		else var s = p/(2*Math.PI) * Math.asin (c/a);
		if (t < 1) return -.5*(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
		return a*Math.pow(2,-10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )*.5 + c + b;
	},
	easeInBack: function (x, t, b, c, d, s) {
		if (s == undefined) s = 1.70158;
		return c*(t/=d)*t*((s+1)*t - s) + b;
	},
	easeOutBack: function (x, t, b, c, d, s) {
		if (s == undefined) s = 1.70158;
		return c*((t=t/d-1)*t*((s+1)*t + s) + 1) + b;
	},
	easeInOutBack: function (x, t, b, c, d, s) {
		if (s == undefined) s = 1.70158; 
		if ((t/=d/2) < 1) return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b;
		return c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b;
	},
	easeInBounce: function (x, t, b, c, d) {
		return c - jQuery.easing.easeOutBounce (x, d-t, 0, c, d) + b;
	},
	easeOutBounce: function (x, t, b, c, d) {
		if ((t/=d) < (1/2.75)) {
			return c*(7.5625*t*t) + b;
		} else if (t < (2/2.75)) {
			return c*(7.5625*(t-=(1.5/2.75))*t + .75) + b;
		} else if (t < (2.5/2.75)) {
			return c*(7.5625*(t-=(2.25/2.75))*t + .9375) + b;
		} else {
			return c*(7.5625*(t-=(2.625/2.75))*t + .984375) + b;
		}
	},
	easeInOutBounce: function (x, t, b, c, d) {
		if (t < d/2) return jQuery.easing.easeInBounce (x, t*2, 0, c, d) * .5 + b;
		return jQuery.easing.easeOutBounce (x, t*2-d, 0, c, d) * .5 + c*.5 + b;
	}
});

/*
 *
 * TERMS OF USE - EASING EQUATIONS
 * 
 * Open source under the BSD License. 
 * 
 * Copyright © 2001 Robert Penner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 
 * Redistributions of source code must retain the above copyright notice, this list of 
 * conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list 
 * of conditions and the following disclaimer in the documentation and/or other materials 
 * provided with the distribution.
 * 
 * Neither the name of the author nor the names of contributors may be used to endorse 
 * or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED 
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE. 
 *
 */