<?php
/**
 * Created by JetBrains PhpStorm.
 * User: VietDev01
 * Date: 6/4/12
 * Time: 3:39 PM
 * To change this template use File | Settings | File Templates.
 * *Template Name: Sức khỏe
 */
?>
<?php get_header(); ?>
<script type="text/javascript">
    selectNav(5);
</script>
<div class="p-health" id="page">
    <div class="navSub">
            <ul>
                <?php
                $currentLink = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
                $categories = get_categories(array("parent"=>6));
                foreach((Array)$categories as $category)
                {
                    $cat_link = get_category_link( $category->term_id );

                    if (strpos($cat_link, $currentLink)){
                        $currentCat = $category;
                    }
                    ?>
                    <li><a class="men_<? echo $category->term_id ?>" href="<?php echo esc_url( $cat_link );?>"> <?php echo $category->cat_name; ?> </a></li>
                    <?php
                }
                if(empty($currentCat) && !empty($categories)){
                    $currentCat = $categories[0];
                }
                ?>
            </ul>
    </div>
    <?php if(!empty($currentCat)){?>
    <?php  ?>
    <?php if(is_single()) {
        include (TEMPLATEPATH . '/single-health.php');
        $postTemp = $wp_query->post;
        $catte = get_the_category($postTemp->ID);
        $listPost = get_posts(array("category"=>$catte[0]->term_id, "order_by"=>"post_date"));
        if(count($listPost)>1){ ?>
            <br/><br/>
            <h2> Các tin khác: </h2>
            <div class="tvc"></div>
            <div class="latestNews">
                <ul>
                    <?php foreach((array)$listPost as $post){
                    if($post->ID != $postTemp->ID) { ?>
                        <li><a href="<?php the_permalink(); ?>"> <?php echo $post->post_title ?> </a> </li>
                        <?php  }
                } ?>
                </ul>
            </div>
            <?php } ?>
    <?php }else{?>
        <div class="main">
        <h1> <?php echo $currentCat->cat_name; ?> </h1>
        <div class="promoList">
        <ul>
        <?php $listPost = get_posts(array("category"=>$currentCat->term_id,'numberposts'=>26, "order_by"=>"post_date"));
        if(!empty($listPost)){
            $flag =0;?>
           <?php for($i = 0; $i < count($listPost); ++$i){
             $flag++;?>
                <li>
                    <a  href="<?php echo post_permalink($listPost[$i]->ID)?>">
                        <?php
                        if(strlen(get_the_post_thumbnail($listPost[$i]->ID)) > 0){?>
                            <img align="middle"  class="thumb" alt="" <?php echo get_the_post_thumbnail($listPost[$i]->ID);?></img>
                           <br/><p align="center">
                        <?php  echo $listPost[$i]->post_title; }
                        ?>
                    </p>
                    </a>
                   <!-- <p align="justify">< ?php echo $listPost[$i]->post_title; ?></p> -->

                </li>

             <?php if($flag==4){ ?>
                  <span style="top:5px"></span>

               <?php $flag = 0; } } ?>
        <?php } ?>
        <ul>
        </div>
    <?php }} else{
        echo "Hiện tại không có bài viết trong chuyên mục này!";
    }?>
   <p class="cl"></p>
    </div>
        <p class="cl"></p>
    </div>
<script type="text/javascript">
    $(document).ready(function(){
        $(".men_" + "<? if( $catte[0]!= null){ echo $catte[0]->term_id; }else{ echo $currentCat->term_id;} ?>").addClass("active");
    })
</script>
<?php get_footer(); ?>