<?php
/**
 * Created by JetBrains PhpStorm.
 * User: VietDev01
 * Date: 6/22/12
 * Time: 1:39 PM
 * To change this template use File | Settings | File Templates.
 * *Template Name: Công Ty
 */
?>
<?php get_header(); ?>
<script type="text/javascript">
    selectNav(2);
</script>
<div class="p-product" id="page">
    <div class="navSub">
        <ul>
            <?php
            $currentLink = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
            $categories = get_categories(array("parent"=>69,'hide_empty'=>0));
            foreach((Array)$categories as $category)
            {
                $cat_link = get_category_link( $category->term_id );
                if (strpos($cat_link, $currentLink)){
                    $currentCat = $category;
                }
                ?>
                <li><a class="men_<? echo $category->term_id ?>" href="<?php echo esc_url( $cat_link );?>"> <?php echo $category->cat_name; ?> </a></li>
                <?php
            }
            if(empty($currentCat) && !empty($categories)){
                $currentCat = $categories[0];
            }
            ?>
        </ul>
    </div>
    <?php
    //print_r($wp_query->post);
    if(!empty($currentCat)){
        $listPost = get_posts(array("category"=>$currentCat->term_id,"numberposts"=>10000, "order_by"=>"post_date"));
        ?>
        <div class="main">
            <?php

            if(!empty($listPost)){

                ?>

                <?php include (TEMPLATEPATH . '/single-product.php'); ?>

                <?php }else{?>
                <h1>Bài viết đang được cập nhật ...</h1>
                <?php }?>
        </div>
        <p class="cl"></p>
        <?php
    }
    ?>
</div>


<script type="text/javascript">
    function activeSubNav(strobj){
        if ($("#"+strobj).attr('class') != 'active') {
            $("#navSub li ul").slideUp();
            $("#"+ strobj).next().slideToggle();

            $("#navSub li a").removeClass('active');
            $("#" + strobj).addClass('active');

        }
    }
    function conplase(){
        $("#navSub li ul").slideUp();
    }
    $(document).ready(function(){
        $(".men_" + "<? echo $currentCat->term_id ?>").addClass("active");
    })
</script>
<?php get_footer(); ?>