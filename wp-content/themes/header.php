<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?><!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title>
    <?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'VietHungFood' ), max( $paged, $page ) );

	?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
    <noscript>
        For full functionality of this site it is necessary to enable JavaScript.
        Here are the <a href="http://www.enable-javascript.com/" target="_blank">instructions how to enable JavaScript in your web browser</a>.
    </noscript>

<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/plugin.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/common.js"></script>
    <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>

    <![endif]-->


</head>

<body <?php body_class(); ?>>
<div class="wrapper">
        <header>
                <span class="logo">
                    <?php if(ICL_LANGUAGE_CODE == "vi"){ ?>
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php echo bloginfo('template_directory'). '/images/logo_viethung.gif' ?>";></a>
                <?php }else{ ?>
                    <a href="<?php echo esc_url( home_url( '/en/homepage-2/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php echo bloginfo('template_directory'). '/images/logo_viethung.gif' ?>";></a>
                <?php } ?>
                </span>

                <div class="hd_r">
                    <?php if(ICL_LANGUAGE_CODE == "vi"){ ?>
                    <div class="lang">
                        <span>Ngôn ngữ</span>
                        <?php// do_action('icl_language_selector');  ?>
                        <a href="/"><img alt="Viet Nam"  src="<?php echo bloginfo('template_directory'). '/images/lang_vn.gif' ?>"/></a>
                        <a href="/en/homepage-2/"><img alt="English" src="<?php echo bloginfo('template_directory'). '/images/lang_en.gif' ?>"/></a>
                    </div>
                   <div class="search">
                        <form id="searchbox_001807701815261070257:qnxmp5leu0w" action="/search">
                            <input type="hidden" name="cx" value="001807701815261070257:qnxmp5leu0w" />
                            <input type="hidden" name="cof" value="FORID:11" />
                            <input name="q" type="hidden" size="20" />
                            <input class="txf" name="q" placeholder="Từ khóa tìm kiếm ">
                            <input class="btn" name="sa"  type="submit" value="">

                        </form>
                    <?php }else{ ?>
                    <div class="lang">
                        <span>Languages</span>
                        <?php // do_action('icl_language_selector');  ?>
                        <a href="/"><img alt="Tiếng Việt"  src="<?php echo bloginfo('template_directory'). '/images/lang_vn.gif' ?>"/></a>
                        <a href="/en/homepage-2/"><img alt="English" src="<?php echo bloginfo('template_directory'). '/images/lang_en.gif' ?>"/></a>
                    </div>
                   <div class="search">
                        <form id="searchbox_001807701815261070257:qnxmp5leu0w" action="/en/search">
                            <input type="hidden" name="cx" value="001807701815261070257:qnxmp5leu0w" />
                            <input type="hidden" name="cof" value="FORID:11" />
                            <input name="q" type="hidden" size="20" />
                            <input class="txf" name="q" placeholder="Search keyword ">
                            <input class="btn" name="sa"  type="submit" value="">

                        </form>
                    <?php }?>
                        <!--   <form action="http://localhost/search"  >
                    <input class="txf"  placeholder="Từ khóa tìm kiếm ">

                    <!-- <input typden" name="cof" value="FORID:9" />
                    <input type="hidden" name="ie" value="UTF-8" />
                    <input type="hidden" name="hl" value="vi" />
                    <input type=hidden name=domains value="http://www.viethungfood.com">
                           <input class="btn" name="sa"  type="submit" value="">
                        </form>-->
                    </div>
                </div>
    </header>
    <div class="nav" id="nav">
               <?php wp_nav_menu( array('') ); ?>
    </div>

    <?php
   // $categories = get_categories();
    if(ICL_LANGUAGE_CODE == "vi"){
        $categories = get_categories(array("parent"=>5,'hide_empty'=>0));
    }else{
        $categories = get_categories(array("parent"=>37,'hide_empty'=>0));
    }
    if(!empty($categories)){
        foreach((array)$categories as $category){
            $categories_item = get_categories(array("parent"=>$category->cat_ID,'hide_empty'=>0));
       //     if($category->cat_name == "TIN BÀI"){
                if(empty($listPost)){ //Lấy danh sách bài post thuộc category tin bài
                    $listPost = get_posts(array("category"=>$categories_item[1]->term_id,"numberposts"=>4, "order_by"=>"post_date"));//lấy 4 bài mới nhất của mỗi category tin tức
                    $postnews01 = $listPost[0];
                    $postnews02 = $listPost[1];
                    $postnews03 = $listPost[2];
                    $postnews04 = $listPost[3];
                }else{
                    $listPostTemp = get_posts(array("category"=>$categories_item[1]->term_id,"numberposts"=>4, "order_by"=>"post_date"));//lấy 4 bài mới nhất của mỗi category tin tức
                    if(!empty($listPostTemp)){
                        for($i=0;$i<count($listPostTemp);++$i){
                            if($listPostTemp[$i]->post_date > $postnews01->post_date){
                                $postnews04 = $postnews03;
                                $postnews03 = $postnews02;
                                $postnews02 = $postnews01;
                                $postnews01 = $listPostTemp[$i];
                            }else if($listPostTemp[$i]->post_date > $postnews02->post_date){
                                $postnews04 = $postnews03;
                                $postnews03 = $postnews02;
                                $postnews02 = $listPostTemp[$i];
                            }else if($listPostTemp[$i]->post_date > $postnews03->post_date){
                                $postnews04 = $postnews03;
                                $postnews03 = $listPostTemp[$i];
                            }else if($listPostTemp[$i]->post_date > $postnews04->post_date){
                                $postnews04 = $listPostTemp[$i];
                            }
                        }
                    }
                }
    //        }
        }
    }
    //$post = $postnews02;
    //print_r($post);
    //echo get_first_image();
    //$post = $postnews02;
    //print_r($post);
    //echo get_first_image();
    //$post = $postnews03;
    //print_r($post);
    //echo get_first_image();
    //$post = $postnews04;
    //print_r($post);
    //echo get_first_image();
    ?>
    <div class="bannerCover">
        <div class="slideBanner">
            <div class="port_slideshow">
                <div class="port_view_all">
                    <div class="port_view rotateCopy">
                        <a href="<?php echo post_permalink($postnews01->ID)?>"> <?php $post = $postnews01;?> <?php if (class_exists('MultiPostThumbnails')) : MultiPostThumbnails::the_post_thumbnail('post', 'secondary-image'); endif; ?> </a>
                        </div>
                    <div class="port_view rotateCopy">
                        <a href="<?php echo post_permalink($postnews02->ID)?>"> <?php $post = $postnews02;?> <?php if (class_exists('MultiPostThumbnails')) : MultiPostThumbnails::the_post_thumbnail('post', 'secondary-image'); endif; ?> </a>
                    </div>
                    <div class="port_view rotateCopy">
                        <a href="<?php echo post_permalink($postnews03->ID)?>"> <?php $post = $postnews03;?> <?php if (class_exists('MultiPostThumbnails')) : MultiPostThumbnails::the_post_thumbnail('post', 'secondary-image'); endif; ?> </a>
                    </div>
                    <div class="port_view rotateCopy">
                        <a href="<?php echo post_permalink($postnews04->ID)?>"> <?php $post = $postnews04;?> <?php if (class_exists('MultiPostThumbnails')) : MultiPostThumbnails::the_post_thumbnail('post', 'secondary-image'); endif; ?> </a>
                    </div>
                </div>
            </div>
            <a href="#" class="prev"></a>
            <a href="#" class="next"></a>
            <div class="mask"></div>
            <div class="disableLayer"></div>
        </div>
        <div class="slideNav">
            <ul>
                <li><a href="#" class="active"><?php echo $postnews01->post_title; ?></a></li>
                <li><a href="#"><?php echo $postnews02->post_title; ?></a></li>
                <li><a href="#"><?php echo $postnews03->post_title; ?></a></li>
                <li><a href="#"><?php echo $postnews04->post_title; ?></a></li>
            </ul>
        </div>
    </div>



	<div id="main">