<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'viethungfood');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '}yN6(d0~o8Rvqyw`-hnJ@Y1c_r?j!qc_[~&<_>;z@UxX`AI!2~U[cVB8;0&|8h_$');
define('SECURE_AUTH_KEY',  'ibloH$s=U@^)shTy*xd*Hy%{(W-[LUR5w(j?2Ie{m8Z>WmM{&/b>R2xt;7m%cJ55');
define('LOGGED_IN_KEY',    'Jq(>jkY1>{m8OXEc,W`mSOIk:2TB(x:VxBm.Fjgf|h}nrOtz%>L|[(bO+ AO%IY3');
define('NONCE_KEY',        'qH[`fDjd*TzW(qurmpgI3p;sVzLgcsoO[A{8OpxItHfX:M/$.QjXn*$&^|iPdrE^');
define('AUTH_SALT',        '}/6t[Iop~2*C0)`y#!*)MSab&CIkry.gb){qs[c%cV;UKo1$4H/}H^bzJr`v-paF');
define('SECURE_AUTH_SALT', 'y4mm&7dmrefifT]:+VSjKFjN`5Bnz9Ci(!0PXRMalWzp6xl]L7EM5zFmpZuF<vJq');
define('LOGGED_IN_SALT',   'r_i3IOB`pEy7#I5Itwwe8?xHO+JRX_k;Vx3wPWZ0vuI7,9FM,GRFWHf!y7zB<YnT');
define('NONCE_SALT',       'UyS,fR?|d$r=&yF94;VFtGy & `wy|Xk9XM@<:HgC{C}P:~/,alWh^RbAE&XOJ]{');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_vhf';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
